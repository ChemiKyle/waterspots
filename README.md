# Waterspots

A Python-based **L**aboratory **I**nformation **M**anagement **S**ystem built for use in drinking water laboratories.

## Prerequisites

Note that if you are deploying on a raspberry pi you will have better luck with the `apt` repositories, as `pip` is occassionally too new and will result in errors with `numpy`.

Raspberry pi dependency one-liner:
```bash
sudo apt install python3-numpy python3-matplotlib python3-pandas python3-flask python3-flask-login
```

Ensure you have required python dependencies, note that if you are deploying on a raspberry pi you will have better luck using packages

## Installation  
`git clone` this repo.  
Run `python3 initialize_db.py`, optionally, uncomment the last line to populate a database with parameters and their units required by different test methods, (note this can be done from a `csv` file that I don't believe can be shared due to containing proprietary information).  


## Use  
Run `python3 app.py`.  
Navigate a web browser to either: `http://your-pi-ip-address:5000/home` or `your-pi-hostname.local:5000/home`  
This software is in its very early stages and subject to breaking changes, I do not reccomend implementing it for serious use at this point.

